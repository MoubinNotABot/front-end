import './App.css';
import Navbar from './component/Navbar/Navbar';
import Banner from './component/Banner/Banner'
import Product from './component/Product/Product';
import Info from './component/Info/Info';
import Cart from './component/Cart/Cart';
import Checkout from './component/Order/Order';
import About from './component/About/About';


import {Routes ,Route} from 'react-router-dom';


function App() {
  return (
    <>
      <Navbar/>
    <Routes> 
      <Route path="/" element={<Banner/>}/>
      <Route path="/product" element={<Product/>}/>
      <Route path="/info/:id" element={<Info/>}/>
      <Route path="/cart" element={<Cart/>}/>
      <Route path="/order" element={<Checkout/>}/>
      <Route path="/about" element={<About/>}/>
      </Routes>
    </>
  );
}

export default App;
