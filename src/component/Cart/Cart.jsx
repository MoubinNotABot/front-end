import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { delCart, addCart, minusCart } from '../../redux/action/index';
import { NavLink } from 'react-router-dom';

const Cart = () => {
    const state = useSelector((state) => state.handleCart)

    var total = 0;
    const itemList = (item) => {
        total = total + item.price;
        total = Math.round((total + Number.EPSILON) * 100) / 100
        return (
            <li className='list-group-item d-flex justify-content-between 1h-sm'>
                <div>
                    <h6 className='my-0'>{item.title}</h6>
                </div>
                <span className='text-muted'>{state.filter(x => x.title === item.title).length} × ${item.price} = ${state.filter(x => x.title === item.title).length * item.price}</span>
            </li>
        );
    }


    const allUnique = [
        ...new Map(state.map((item) => [item["title"], item])).values(),
    ];

    const dispatch = useDispatch()
    const handleClose = (item) => {
        dispatch(delCart(item))
    }
    const handlePlus = (item) => {
        dispatch(addCart(item))
    }
    const handlemMinus = (item) => {
        dispatch(minusCart(item))
    }

    const cartItems = (cartItems) => {
        return (
            <div className='px-4 my-5 bg-light rounded-3'>
                <div className='container py-4'>
                    <button onClick={() => handleClose(cartItems)} className='btn-close float-end' aria-label='Close'></button>
                    <div className='row justify-content-center'>
                        <div className='col-md-4'>
                            <img src={cartItems.image} alt={cartItems.title} height='200px' width='180px' />
                        </div>
                        <div className='col-md-4'>
                            <h3>{cartItems.title}</h3>
                            <p className='lead fw-bold'>
                                {state.filter(x => x.title === cartItems.title).length} × ${cartItems.price} = ${state.filter(x => x.title === cartItems.title).length * cartItems.price}
                            </p>
                            <button className="btn btn-outline-dark me-4" onClick={() => handlemMinus(cartItems)}><i className='fa fa-minus'></i></button>
                            <button className="btn btn-outline-dark me-4" onClick={() => handlePlus(cartItems)}><i className='fa fa-plus'></i></button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    const orderButton = () => {
        return (
            <div className='container'>
                <div className='row'>
                    <NavLink to="/checkout" className="btn btn-success">Place Order</NavLink>
                </div>
            </div>

        );
    }
    const cancelButton = () => {
        return (
            <div className='container'>
                <div className='row'>
                    <NavLink to="/checkout" className="btn btn-dark">Cancel</NavLink>
                </div>
            </div>

        );
    }

    return (
        <>
        
        <div class="row">
        <div className='col-md-6 order-md-1' id="cartContent">
                {state.length !== 0 && allUnique.map(cartItems)}
            </div>
            
            <div className="col-md-6 order-md-1" id="checkContent">
                <ul className="list-group mb-3">
                    
                {state.length !== 0 && orderButton()}
                    {allUnique.map(itemList)}
                    
                    <li className='list-group-item d-flex justify-content-between'>
                        <span>Total (USD)</span>
                        <strong>${total}</strong>
                    </li>
                </ul>

                    

                    {state.length !== 0 && cancelButton()}

            </div>

        </div>
        </>
    );
}

export default Cart;