import React from 'react'
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

import "./Navbar.scss";
const Navbar = () => {
        const state = useSelector((state)=>state.handleCart)
    return (
        <div>
            <nav className="navbar navbar-expand-lg bg-light" id="header">
                <div className="container-fluid">
                    <div className="logo">
                        <NavLink to="/">
                            TEAM 2
                        </NavLink>
                    </div>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                            <li className="nav-item">
                                <NavLink to="/product" className="nav-link" >Products</NavLink>
                            </li>

                            <div class="input-group rounded">
                                <input type="search" class="form-control rounded" placeholder="Search Product" aria-label="Search" aria-describedby="search-addon" />
                                <span class="input-group-text border-0" id="search-addon">
                                <button type="button" class="btn btn-success">Search</button>
                                </span>
                            </div>
                        </ul>
                        <div className="button">
                            <NavLink to="/login" className="btn btn-outline-dark">
                                <i className='fa fa-sign-in me-1'></i>Login</NavLink>
                            <NavLink to="signup" className="btn btn-outline-dark ms-2">
                                <i className='fa fa-user-plus me-1'></i>Sign-Up</NavLink>
                            <NavLink to="cart" className="btn btn-outline-dark ms-2">
                                <i className='fa fa-cart-plus me-1'></i> ({state.length})</NavLink>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default Navbar;