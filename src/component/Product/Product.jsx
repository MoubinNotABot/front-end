import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom';

const Product = () => {
    const [data, setData] = useState([])
    const [filter, setFilter] = useState(data);
    const [loading, setLoading] = useState(false);
    let componentMounted = true;

    useEffect(() => {
        const getProducts = async () => {
            setLoading(true)
            const response = await fetch("https://fakestoreapi.com/products");
            if (componentMounted) {
                setData(await response.clone().json());
                setFilter(await response.json());

            }
            console.log(componentMounted);
        }
        getProducts();
    }, []);

    const ShowProducts = () => {
        return (
            <>
                {filter.map((product) => {
                    return (
                        <>
                            <div className='col-md-3'>
                                <div className="card text-center p-5" key={product.id}>
                                    <img src={product.image} className="card-img-top" alt={product.title} height="250px"/>
                                        <div className="card-body fw-bolder">
                                            <h5 className="card-title">{product.title.substring(0,12)}..</h5>
                                            <p className="card-text">${product.price}</p>
                                            <NavLink to={`/info/${product.id}`} className="btn btn-outline-info me-2">Detail</NavLink>
                                        </div>
                                </div>
                            </div>
                        </>
                    );
                })}
            </>
        )

    }
    return (
        <div>
            <div className="container my-5 ">
                <div className='row'>
                    <div className='col-12 mb-5'>
                        <h1 className='fw-bolder text-center'>Products</h1>
                    </div>
                </div>
                <div className='row justify-content-center'>
                    <ShowProducts />
                </div>
            </div>
        </div>
    )
}

export default Product;